var _ = require('underscore');

// # recursiveDefaults
// ==================
// Recursively add default values to the base object. Only keys
// that are not present in `base` or any of it's child objects
// will be added.
exports.recursiveDefaults = function recursiveDefaults(base, source) {
    // base check
    if (!_.isObject(base)) {
        return base;
    }

    // Append all source values to the base
    _.each(source, function(value, key) {
        if (!base[key]) {
            base[key] = value;
        }
        else if (_.isObject(value)) {
            base[key] = recursiveDefaults(base[key], source[key]);
        }
    });

    return base;
};
