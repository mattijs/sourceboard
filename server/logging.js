var winston = require('winston');

// Simple write method for winston Logger logger
winston.Logger.prototype.write = winston.Logger.prototype.log;

// The logger singleton
var logger = null;

// Configure the Logger. Can only be called once.
exports.configure = function(config) {
    if (logger) {
        throw new Error("Logger can only be configured once");
    }

    if (!config.name || 0 >= config.name.length) {
        throw new Error("Logger requires a name");
    }

    var level  = config.level || 'info';
    logger = new winston.Logger();

    // Add console logging?
    if (config.stdout || config.console) {
        logger.add(winston.transports.Console);
        logger.cli();
    }

    // Add log file?
    if (config.file) {
        logger.add(winston.transports.File, { filename: config.file, level: level });
    }

    return logger;
};

// Return the configured logger
Object.defineProperty(exports, 'logger', {
    get: function() {
        if (!logger) {
            throw new Error('Logger not configured');
        }
        return logger;
    },
    configurable: false
})
