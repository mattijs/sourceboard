var _ = require('underscore'),
    express = require('express'),
    faye    = require('faye');


// # Server
// ========
// This server wrapper to bring together express and faye websockets
// and provide a simple external API.
function Server(options) {
    // Store configuration
    this.config = config = options;

    // Create a new express app
    this.app = app = express.createServer();

    // Configure it
    app.configure(function() {
        app.use(express.methodOverride());
        app.use(express.bodyParser());
        app.use(app.router);

        // Check if we need to serve static files
        if (options.webroot) {
            app.use(express.static(config.webroot));
        }
    });

    // Check if we need to create a websocket host
    if (config.websocket) {
        // Attach WebSocket capabilities to the Express app
        this.bayeux = new faye.NodeAdapter(config.websocket);
        this.bayeux.attach(app);
    }
}
exports.Server = Server;

// # start
// ========
// Simple start method that will call `listen` on the internal express
// server with the configure host and port.
Server.prototype.start = function() {
    this.app.listen(this.config.port, this.config.host);
};

// # publish
// =========
// Publish data over the websocket channel if it was configured. This
// will fail silently if no websocket connection was available.
Server.prototype.publish = function(data, channel) {
    channel = channel || this.config.websocket.channel;
    this.bayeux.getClient().publish(channel, data);
};

// # attachUrlHandler
// ==================
// Attach a callback to an URL
Server.prototype.attachRequestHandler = function(options) {
    // Add default method
    options = _.defaults(options, { method: 'get' });

    // Attach the handler to the internal express application
    // through the VERB call (router is not exposed)
    this.app[options.method.toLowerCase()](options.url, options.handler);

    return this;
};

// # create
// ========
// Create an express app for serving the client
// and the API
exports.create = function(options) {
    return new Server(options);
};

