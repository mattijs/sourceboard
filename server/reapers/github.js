var util = require('util'),
    events = require('events'),
    request = require('request'),
    moment = require('moment'),
    logger = require('../logging').logger;

var PROVIDER_NAME = 'github';
var API_BASE = "https://api.github.com";
var DEFAULT_INTERVAL = 60 * 1000;

// # GithubOrganizationReaper
// ==========================
// Reaper for collecting data from a GitHub Organization
function GithubOrganizationReaper(config) {
    events.EventEmitter.call(this);

    // Pluck config
    this.user  = config.user;
    this.org   = config.org;
    this.token = config.token;

    // Update interval configuration
    this.interval = config.interval || DEFAULT_INTERVAL;
    this.intervalHandle = null;
}
util.inherits(GithubOrganizationReaper, events.EventEmitter);

// # source
// ========
// Source handle for the reaper. This is used when dealing with
// reaper data.
GithubOrganizationReaper.prototype.source = PROVIDER_NAME;

// # start
// ===============
// Start reaping, harvesting data through the GitHub API. This method sets an interval
// and calls the update method to retreive the data from the GitHub API.
GithubOrganizationReaper.prototype.start = function() {
    logger.info('Starting GitHub reaping for Organization ' + this.org);
    // Set an interval for updating
    this.intervalHandle = setInterval(this.update.bind(this), this.interval);

    // Do an initial update
    this.update();

    return this;
};

// # update
// ========
// Initiate a request to fetch updated data
GithubOrganizationReaper.prototype.update = function() {
    var self = this;
    logger.debug('Fetching GitHub update');

    // Build and execute the HTTP request
    request({
        method:  "GET",
        uri:     API_BASE + "/users/" + this.user + "/events/orgs/" + this.org,
        headers: { "Authorization": "token " + this.token }
    }, function (error, response, body) {
        if (error) {
            logger.error('An error occurred while updating GitHub actions');
            logger.data(error);
            return;
        }
        // @TODO Proper response handling and checking

        // Update Actions
        self.process(JSON.parse(body));
    });
};

// # process
// =========
// Process update data. This will emit the 'update' event with the processed data attached
// for each action found in the update data.
GithubOrganizationReaper.prototype.process = function(actions) {
    actions.forEach(function(data) {
        // Construct the data to emit
        var emitData = {
            id:        data.id,
            date:      moment(data.created_at).toDate(),
            provider:  PROVIDER_NAME,
            type:      data.type,
            meta:      data
        };

        // Emit a `data` event
        this.emit('data', emitData, this);
    }, this);
};

// our awesome export products
exports = module.exports = GithubOrganizationReaper;
