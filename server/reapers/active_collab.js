var util   = require('util'),
    events = require('events');

var PROVIDER_NAME = 'active_collab';

// ActiveCollabReaper
// ==================
// Reaper collecting ticket information from an Active Collab instance
function ActiveCollabReaper(options) {
    events.EventEmitter.call(this);
}
util.inherits(ActiveCollabReaper, events.EventEmitter);

// # source
// ========
// Source handle for the reaper.
ActiveCollabReaper.prototype.source = PROVIDER_NAME;

// our awesome export products
exports = module.exports = ActiveCollabReaper;
