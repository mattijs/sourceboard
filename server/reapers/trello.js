var util   = require('util'),
    events = require('events'),
    request = require('request'),
    moment = require('moment'),
    logger = require('../logging').logger;

// Some default values
var PROVIDER_NAME =    'trello';
var API_BASE =         'https://api.trello.com/1';
var DEFAULT_INTERVAL = 60 * 1000;

// # TrelloBoardReaper
// ==========================
// Reaper for collecting data from a Trello Board
function TrelloBoardReaper(config) {
    events.EventEmitter.call(this);

    // Pluck config
    this.board = config.board;
    this.key   = config.key;
    this.token = config.token;

    // Update interval config
    this.interval = config.interval || 5 * 60 * 1000;
    this.intervalHandle = null;
}
util.inherits(TrelloBoardReaper, events.EventEmitter);

// # source
// ========
// Source handle for the reaper. This is used when dealing with reaper data.
TrelloBoardReaper.prototype.source = PROVIDER_NAME;

// # start
// ===============
// Start reaping, harvesting data through the Trello API. This method sets an interval
// and calls the update method to retreive the data from the Trello API.
TrelloBoardReaper.prototype.start = function() {
    logger.info('Starting Trello reaping for Board ' + this.board);
    // Set an interval for updating
    this.intervalHandle = setInterval(this.update.bind(this), this.interval);

    // Do an initial update
    this.update();

    return this;
};

// # update
// ========
// Initiate a request to fetch updated data for the configured board
TrelloBoardReaper.prototype.update = function() {
    var self = this;
    logger.debug('Fetching trello update');

    request({
        uri: API_BASE + "/board/" + this.board + "/actions",
        qs:  { key: this.key, token: this.token }
    }, function (error, response, body) {
        if (error) {
            logger.error('an error occurred while updating trello actions');
            logger.data(error);
            return;
        }

        // @TODO proper response handling and checking
        if (response.statusCode != 200) {
            console.log('Trello responded with status ' + response.statusCode);
            console.log(body);
            return;
        }

        // update actions
        self.process(JSON.parse(body));
    });
};

// # process
// =========
// Process update data. Additional card information will be fetched for each
// action received. Once all data is gathered an `update` event with the complete
// data set attached will be emitted for each action.
TrelloBoardReaper.prototype.process = function(actions) {
    actions.forEach(function(data) {
        // Construct the data to emit
        var emitData = {
            id:        data.id,
            date:      moment(data.date).toDate(),
            provider:  PROVIDER_NAME,
            type:      data.type,
            meta:      data
        };

        // Emit a `data` event
        this.emit('data', emitData, this);
    }, this);
};

// our awesome export products
exports = module.exports = TrelloBoardReaper;
