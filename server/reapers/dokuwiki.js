var util   = require('util'),
    events = require('events');

var PROVIDER_NAME = 'dokuwiki';

// DokuwikiReaper
// ==============
// Reaper collecting edit information from a DokuWiki instance
function DokuwikiReaper(options) {
    events.EventEmitter.call(this);
}
util.inherits(DokuwikiReaper, events.EventEmitter);

// # source
// ========
// Source handle for the reaper.
DokuwikiReaper.prototype.source = PROVIDER_NAME;

// our awesome export products
exports = module.exports = DokuwikiReaper;

