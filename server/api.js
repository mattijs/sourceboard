var _ = require('underscore'),
    moment = require('moment'),
    strwrp = require('stringwarp');

// # ApiHandler
// ============
// API Handler for Combine. Will expose Combine data through the web server.
function ApiHandler(server, combine) {
    this.server = server;
    this.combine = combine;

    // Default page size for paginated URL's
    this.per_page = 30;

    // Register handlers
    this.register();
}


// # register
// ==========
// Register our request handlers with the web server
ApiHandler.prototype.register = function() {
    this.server.attachRequestHandler({
        method:  'get',
        url:     '/actions',
        handler: this.getActions.bind(this)
    });
};


// # getActions
// ===========
// Get a list of Actions. A set of 20 actions is returned. The start of the
// set can be controlled by the `?page=<int>` query string parameter.
ApiHandler.prototype.getActions = function(request, response) {
    // Get the page number
    var page = request.param('page', 1);

    // Get the actions from the Combine
    var actions = this.combine.getActionSet(this.per_page, page);
    var actionCount = this.combine.getActionCount();

    // Get page links
    var links = this.getPageLinks({
        total:    actionCount, 
        page:     page,
        url:      '/actions'
    });

    // Build response message
    var message = { body: actions };
    if (0 < _.toArray(links).length) {
        message.headers = { 'Link': _.toArray(links).join(', ')};
    }

    // Send response as JSON(P)
    this.sendJSON(message, response, request);
};

// # getPageLinks
// ==============
ApiHandler.prototype.getPageLinks = function(options) {
    // Add defaults to options
    options = _.defaults(options, { per_page: this.per_page, page: 1, total: 0, param: 'page' });
    var links = {};

    // Extract and convert numbers
    var total    = parseInt(options.total, 10),
        per_page = parseInt(options.per_page, 10),
        cur_page = parseInt(options.page, 10);

    // Safeguard agains empty totals and negative page numbers
    if (0 >= total) {
        return links;
    }
    if (cur_page < 1) {
        cur_page = 1;
    }

    // Link template
    var templ = '<#{url}?#{param}=#{num}>; rel=#{rel};';

    // Calculate the number of pages for the setup
    var number_of_pages = Math.ceil(total / per_page);

    // `next` link
    if (cur_page < number_of_pages) {
        links.next = strwrp.intpol(templ, {
            rel: 'next',
            num: cur_page + 1,
            url: options.url,
            param: options.param
        });
    }

    // `last` link
    if (1 < number_of_pages && cur_page !== number_of_pages) {
        links.last = strwrp.intpol(templ, {
            rel: 'last',
            num: number_of_pages,
            url: options.url,
            param: options.param
        });
    }

    // `prev` link
    if (1 < number_of_pages && 1 < cur_page) {
        links.prev = strwrp.intpol(templ, {
            rel: 'prev',
            num: cur_page - 1,
            url: options.url,
            param: options.param
        });
    }

    // `first` link
    if (1 < number_of_pages && 1 !== cur_page) {
        links.first = strwrp.intpol(templ, {
            rel: 'first',
            num: 1,
            url: options.url,
            param: options.param
        });
    }

    return links;
};

// # sendJSON
// ==========
// Convenience method for sending a JSON(P) response to the client. This closes the 
// connection making the response Object unusable.
//
// Before the response is send a check is made on the request Object to detect
// a JSONP callback. When a callback parameters is detected in the querystring
// the response data is wrapped in a function.
ApiHandler.prototype.sendJSON = function(message, response, request) {
    // Extract body and headers from the message
    var body = message.body || ''
      , headers = message.headers || {};

    // Check if the request is a JSONP request
    var JSONPCallback = request.query['callback'];
    var isJSONP = !!JSONPCallback;

    // Add Content-Type headers if not present
    if (!headers['Content-Type']) {
        var ctype = (isJSONP) ? 'appication/javascript' : 'application/json';
        headers['Content-Type'] = ctype + '; charset=utf-8';
    }

    // Add Expires header
    if (!headers['Expires']) {
        var expires = moment().add('m', 1);
        headers['Expires'] = expires.toDate();
    }

    // Convert JSON for transport
    if ('string' !== typeof(body)) {
        body = JSON.stringify(body);
    }

    // Wrap JSONP requests in a function call
    if (isJSONP && JSONPCallback) {
        body = JSONPCallback  + '(' + body + ');';
    }

    // Send the response
    response.send(body, headers, 200);
};

// # create
// ========
// Create an API handler
exports.create = function(web, combine) {
    return new ApiHandler(web, combine);
};
