# Source Board

Source Board is an application consisting of a server and a client to collect and show
events and actions from externals systems.

Connections with external systems are made through `reapers` that collect the information
from the external systems. This information is passed to the client through a websocket
connection for an instant update.

## Reapers

The following reapers are available:

- Github Organisation - Collect events for a GitHub Organization
- Trello Board - Collect information for a Trello Board
- Active Collab - Skelleton only
- Dokuwiki - Skeleton only

# Running

The server can be started by running the `server.js` file with node. This script takes
the path to a configuration file as the first argument. A sample configuration can
be found in `config.json.sample`.

# Configuration

See `config.json.sample` for now. Detailed information will follow soon.

# License

Source Board is available under the MIT license. See the `LICENSE` file for the
complete license.
