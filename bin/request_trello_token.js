#!/usr/bin/env node
// Small script to let you request an application token from Trello.
// This script requires your API key to request an application token for
// SourceBoard. Only read access is required for now.
// A new browser window will open to confirm the authorization request.
// The token shown after approval can be copied to the server configuration
// file.
//
// Optionally a expiration length in days can be passed as a second argument.
//
// Your Application Key used as the first argument to this script can be
// fetched from:
//
//   https://trello.com/1/appKey/generate.
//
//
// The URL template used for the authorization request:
//
//   https://trello.com/1/authorize?key=ApplicationKey&name=SourceBoard&expiration=ExpirationDays&response_type=token&scope=read

var path = require('path'),
    url  = require('url'),
    spawn = require('child_process').spawn;

// Check script arguments
var args = Array.prototype.slice.call(process.argv, 2);
if (1 > args.length) {
    console.log('Missing KEY argument');
    console.log('Usage: ' + path.basename(process.argv[1]) + ' KEY [ EXPIRATION ]');
    console.log('  KEY        - Your API key');
    console.log('  EXPIRATION - Optional integer for token expiration in days.');
    process.exit();
}

var apiKey     = args.shift();
var expiration = parseInt(args.shift(), 10) || 30;
var appName    = 'Source Board';

// Build the URL properties
var urlProps = {
    protocol: 'https',
    host:     'trello.com',
    pathname: '/1/authorize',
    query:    {
        key:           apiKey,
        name:          appName,
        scope:         'read',
        expiration:    expiration + 'days',
        response_type: 'token'
    }
};

var apiUrl = url.format(urlProps);
console.log('Opening ' + apiUrl);

// Open is specific to each platform
switch(process.platform) {
    case 'darwin':
        spawn('open', [apiUrl], { stdio: ['ignore', 'pipe', 'pipe'] });
    break;
    default:
        console.log('Unsupported platform');
    break;
}
