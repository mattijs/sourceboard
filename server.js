var fs   = require('fs'),
    path = require('path'),
    http = require('http'),
    express = require('express'),
    faye = require('faye'),
    _ = require('underscore');

// Simple argument check
if (process.argv.length < 3) {
    console.log('server.js requires a config path');
    process.exit(1001);
}

// Config
var configData = fs.readFileSync(process.argv[2], 'utf8');
var config = JSON.parse(configData);

// Some (sensible?) defaults for logging and the web server
var rd = require('./server/utils').recursiveDefaults;
config = rd(config, {
    "logging": {
        "name":   "source",
        "stdout": false
    },
    "server": {
        "port":    8081,
        "host":    "127.0.0.1",
        "webroot": "./client/app",
        "websocket": {
            "mount": "/faye",
            "timeout": 45,
            "channel": "data"
        }
    },
    "combine": {
        "interval": 30000,
        "reapers": []
    }
});

// Set up logging
var logger = require('./server/logging').configure(config.logging);

// Create a new Combine
var combine = require('./server/combine').create(config.combine);

// Create a new web server
var web = require('./server/web').create(config.server);
combine.on('action', function(action) {
    // Only send the JSON representation of the model down the line
    web.publish(action.toJSON(), 'actions');
});

// Attach an API handler to the web server
var api = require('./server/api').create(web, combine);

// Start the combine harvester
combine.start();
logger.info('Combine harvester started');

// Start the web server
web.start();
logger.info('WEB: server listening on port http://' + web.config.host + ':' + web.config.port);
logger.debug('WEB: webroot at ' + web.config.webroot);
logger.debug('WEB: websocket at ' + web.config.websocket.mount);
