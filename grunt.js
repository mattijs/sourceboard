module.exports = function(grunt) {
    // Initialize build configbuild
    grunt.initConfig({
        less: {
            compile: {
                options: {
                    paths: [ "/css" ]
                },
                files: {
                    "app/public/css/source.css": [ 'app/less/elements.less' ]
                }
            },
            files: {
                core: [ 'app/less/elements.less' ],
                all:  [ 'app/*.less' ]
            }
        },
        watch: {
            less: {
                files: '<config:less.files.core>',
                tasks: 'less:compile'
            }
        }
    });

    // Load additional tasks
    grunt.loadNpmTasks('grunt-contrib');
};
