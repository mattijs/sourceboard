// Define the application bootstrap. This is done with 
// an explicit define and require to allow config loading
// from the require.js config structure. With the `main`
// a config cannot be assigned as the module will be 
// anonymous.
define(
    'main',
    ['module', 'jquery', 'underscore', 'utils', 'app/board'],
    function(module, $, _, utils, Board) {

    // Load app config
    var configKeys = ['server', 'websocket'];
    var config = _.chain(module.config() || {})
         .pick(configKeys)
         .tap(function(obj) {
             return utils.recursiveDefaults(obj, gatherDefaults());
         })
         .value();

    // Create a new application
    var app = window.app = new Board(config);

    // Initialize the app when the DOM is fully loaded
    $(function() {
        app.initialize();
    });

    // Set up some defaults for the main app.
    function gatherDefaults() {
        var loc = window.location;
        return {
            server: {
                protocol: loc.protocol.substring(0, loc.protocol.length - 1),
                host: loc.hostname,
                port: loc.port || 80
            },
            websocket: {
                path: '/faye',
                timeout: 45
            }
        };
    }
});

// Trick require.js and load the main module here so the 
// defined config will be available to it.
require(['main']);
