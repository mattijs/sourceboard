define(
    ['jquery', 'faye', 'utils', 'app/actions', 'app/api'],
    function($, Faye, utils, actions, ApiClient) {

    // Board
    // =====
    function Board(options) {
        this.options = options;

        // Collected actions
        this.actions = new actions.ActionCollection();

        // Websocket client
        this.wsClient = null;

        // API client
        this.apiClient = new ApiClient(this.options.server);
    }

    // intialize
    // =========
    Board.prototype.initialize = function() {
        // Create a WebSocket client
        var uri = [
            this.options.server.protocol, '://',
            this.options.server.host, ':', this.options.server.port,
            this.options.websocket.path
        ].join('');
        this.wsClient = new Faye.Client(uri);

        // Subscribe to the `/actions` channel
        this.wsClient.subscribe('/actions', this.processActions.bind(this));

        // Connect an intial list of actions
        var self = this;
        this.apiClient.getActions({ number: 100 }, function(error, actions) {
            // @TODO Implement error handling
            if (!error) {
                self.processActions(actions);
            }
        });
    };

    // processActions
    // ==============
    Board.prototype.processActions = function(actions) {
    };

    return Board;
});
