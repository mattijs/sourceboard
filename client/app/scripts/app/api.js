define(['jquery', 'underscore', 'utils'], function($, _, utils) {

    // ApiClient
    // =========
    function ApiClient(options) {
        this.options = _.chain(options)
            .pick('host', 'port', 'protocol')
            .defaults({
                crossDomain: false
            })
            .value();

        // Alias to the jQuery method to use for retreiving server data
        this.getData = $[(!this.options.crossDomain) ? 'getJSON' : 'getJSONP'];
    }

    // Extend the API client
    _.extend(ApiClient.prototype, {
        // Get a list of actions
        getActions: function(options, callback) {
            options = options || {};

            // Construct the parameters for building the URI
            var uriParams = utils.getUriParams(this.options);
            uriParams.path = '/actions';
            // Add paging
            if (options.page) {
                uriParams.querystring.page = options.page || 1;
            }
            // Add specific number of actions to return
            if (options.number) {
                uriParams.querystring.number = options.number;
            }

            // Get the actions data
            this.getData(utils.buildUri(uriParams), function(data, textStatus, jqXHR) {
                // Server Error
                if (jqXHR.status == 500) {
                    return callback(new Error('An internal server error ocurred while retrieving the list of actions'), []);
                }
                else if (jqXHR.status != 200) {
                    return callback(new Error('A unknown error ocurred while retrieving the list of actions'), []);
                }

                return callback(null, data);
            });
        }
    });

    // our awesome export products
    return ApiClient;
});
