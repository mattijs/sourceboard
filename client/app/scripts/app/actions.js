define(['backbone'], function(Backbone) {
    // Action Model
    // ------------
    var Action = Backbone.Model.extend();

    // Action Collection
    // -----------------
    var ActionCollection = Backbone.Collection.extend({
        model: Action,
        comparator: function(a, b) {
            var aTime = a.get('date'),
                bTime = b.get('date');

            if (aTime > bTime) {      return -1; }
            else if (aTime < bTime) { return 1;  }
            else {                    return 0;  }
        }
    });

    // our awesome export products
    return {
        ActionModel:      Action,
        ActionCollection: ActionCollection
    };
});
