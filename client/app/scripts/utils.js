// Utilities
// =========
define('utils', ['underscore'], function(_) {

    // Recursively add default values to an object.
    function recursiveDefaults(base, source) {
        if (!_.isObject(base)) {
            return base;
        }

        // Append all source values to the base
        _.each(source, function(value, key) {
            if (!base[key]) {
                base[key] = value;
            }
            else if (_.isObject(value)) {
                base[key] = recursiveDefaults(base[key], source[key]);
            }
        });

        return base;
    }

    // Return a map with specific URI parameters from an options Object.
    // This map can be used for building an URI with `buildUri`.
    function getUriParams(options) {
        return _.chain(options)
                .pick('host', 'port', 'protocol', 'querystring', 'path')
                .defaults({ querystring: {} })
                .value();
    }

    // Builds a URI string from an `options` map.
    function buildUri(options) {
        options = getUriParams(options || {});

        // Build the first part of the URI
        var uri = [options.protocol, '://', options.host, ':', options.port, options.path].join('');

        // Check for a querystring map
        if (options.querystring && _.isObject(options.querystring)) {
            var qs = _(options.querystring).map(function(value, key) {
                return key + '=' + value;
            });

            // Append the querystring to the constructed URI
            uri += '?' + qs.join('&');
        }

        return uri;
    }

    // our awesome export products
    return {
        recursiveDefaults: recursiveDefaults,
        getUriParams: getUriParams,
        buildUri: buildUri
    };
});
