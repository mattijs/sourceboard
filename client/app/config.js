// Configure require.js
require.config({
    // module configuration (app specific)
    config: {
        "main": {
            server: {
                host: 'localhost',
                port: 8081
            },
            websocket: {
                path: '/faye',
            }
        }
    },

    // By default load scripts from the vendor directory
    baseUrl: 'scripts/vendor',

    // deps path is relative to baseUrl
    deps: ['../main'],

    // path setup, relative to baseUrl
    paths: {
        // Special paths for application code
        'app':        '../app',
        'css':        '../css',
        'less':       '../less',
        'utils':       '../utils',

        // Explicitely add paths to provide versioning to vendor 
        // libraries (personal preference) until volo is used.
        'jquery':     'jquery-1.7.2.min',
        'backbone':   'backbone-0.9.2.min',
        'underscore': 'underscore-1.3.3.min',
        'handlebars': 'handlebars-1.0.0.beta.6',
        'faye':       'faye-browser-0.8.2.min',
        'moment':     'moment-1.6.2.min'
    },

    // shim config for non-requirejs compatible scripts
    shim: {
        'underscore': {
            exports: '_'
        },
        'backbone': {
            deps:    ['underscore', 'jquery'],
            exports: 'Backbone'
        },
        'faye': {
            exports: 'Faye'
        }
    }
});


